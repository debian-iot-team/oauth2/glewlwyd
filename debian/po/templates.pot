# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the glewlwyd package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: glewlwyd\n"
"Report-Msgid-Bugs-To: glewlwyd@packages.debian.org\n"
"POT-Creation-Date: 2021-09-07 12:04-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. Type: select
#. Default
#. Translators: Default config type, but not translated (or is it?)
#: ../templates:1001 ../templates:1002
msgid "Personalized"
msgstr ""

#. Type: select
#. Choices
#: ../templates:1001
msgid "No configuration"
msgstr ""

#. Type: select
#. Description
#: ../templates:1003
msgid "Glewlwyd setup"
msgstr ""

#. Type: select
#. Description
#: ../templates:1003
msgid "You can configure it later if needed"
msgstr ""

#. Type: string
#. Description
#: ../templates:2001
msgid "External address to access Glewlwyd:"
msgstr ""

#. Type: note
#. Description
#: ../templates:3001
msgid "PostgreSQL requires pgcrypto"
msgstr ""
